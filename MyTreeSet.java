package TreeSetPackage;

import java.util.AbstractSet;
import java.util.Collection;
import java.util.Iterator;

/**
 * Created by ngumin01 on 29/03/2017.
 */
public class MyTreeSet<T> extends AbstractSet<T> {
    private BinarySearchTree<T> tree;

    public MyTreeSet() {
        super();
        this.tree = new BinarySearchTree<>();
    }

    public MyTreeSet(Collection<? extends T> collection) {
        super();
        this.tree = new BinarySearchTree<>();
        this.addAll(collection);
    }

    @Override
    public boolean add(T t) {
        return this.tree.insert(t);
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {

            private Node<T> getInitialStartNode() {
                Node<T> cursorNode = MyTreeSet.this.tree.getRoot();
                if (cursorNode == null)
                    return null;
                while (cursorNode.left != null)
                    cursorNode = cursorNode.left;

                return cursorNode;
            }

            Node<T> tempNode = this.getInitialStartNode();

            @Override
            public boolean hasNext() {
                return tempNode != null;
            }

            @Override
            public T next() {
                return null;
            }
        };
    }

    @Override
    public int size() {
        return this.tree.size();
    }

    public void print() {
        this.tree.traverse(this.tree.getRoot());
    }

    private class BinarySearchTree<K> {
        private Node<K> root;
        private int leaves = 0;

        public BinarySearchTree() {
            this.root = null;
        }

        public boolean insert(K value) {
            if (value == null)
                throw new NullPointerException("Inserting null object is not allowed.");

            boolean res = false;
            if (this.root == null) {
                Node<K> newNode = new Node<>(value);
                this.root = newNode;
                res = true;
            } else {
                res = root.insert(value);
            }

            if (res)
                leaves++;
            return res;
        }

        public int size() {
            return this.leaves;
        }

        public Node<K> getRoot() {
            return this.root;
        }

        public void traverse(Node<K> root) {
            if (root == null)
                return;

            this.traverse(root.left);
            System.out.print(root.val.hashCode() + " ");
            this.traverse(root.right);
            return;
        }
    }

    private class Node<V> {
        public Node<V> parent;
        public Node<V> left;
        public Node<V> right;
        public V val;

        public Node(V value) {
            this.val = value;
            this.left = null;
            this.right = null;
            this.parent = null;
        }

        public boolean insert(V value) {
            if (this.val.hashCode() == value.hashCode())
                return false;
            else if (this.val.hashCode() > value.hashCode()) {
                if (this.left == null) {
                    Node<V> newNode = new Node<>(value);
                    this.left = newNode;
                    newNode.parent = this;
                    return true;
                } else {
                    return this.left.insert(value);
                }
            } else {
                if (this.right == null) {
                    Node<V> newNode = new Node<>(value);
                    this.right = newNode;
                    newNode.parent = this;
                    return true;
                } else {
                    return this.right.insert(value);
                }
            }
        }
    }
}
